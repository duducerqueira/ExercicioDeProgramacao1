#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

class GameObject{
private:
	int x,y;
	char object;

public:
	GameObject();//Construtor
	~GameObject();//Destrutor

	virtual void choosePlayer()=0;
	virtual int getX();
	virtual int getY();
	virtual void setX(int x);
	virtual void setY(int y);
	virtual char getObject();
	virtual void setObject(char o);
};
#endif

