#ifndef BONUS_HPP
#define BONUS_HPP

#include "gameObject.hpp"
#include <iostream>

using namespace std;

class Bonus:public GameObject{
	private:
  		int bonus;
	public:
		Bonus();
		~Bonus();
		int getBonus();
		void setBonus(int bonus);
		void choosePlayer();
};
#endif
