#ifndef DESIGN_HPP
#define DESIGN_HPP

#include "map.hpp"
#include <ncurses.h>

class Design{
	private:
  		bool screen;
	public:
		Design();
  		~Design();
		void setScreen(bool screen);
	  	bool getScreen();
  		void draw(char ** m,Player * jog);
};
#endif
