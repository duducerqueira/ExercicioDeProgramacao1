#ifndef TRAP_HPP
#define TRAP_HPP

#include "gameObject.hpp"

using namespace std;

class Trap:public GameObject{
	private:
  		int loss;
	public:
  		Trap();
  		~Trap();
  		int getLoss();
  		void setLoss(int loss);
  		void choosePlayer();
};
#endif
