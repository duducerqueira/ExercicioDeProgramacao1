#ifndef RECORDS_HPP
#define RECORDS_HPP

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;
class Records{
	private:
  		int quantity;

	public:
  		Records();
  		~Records();
  		void setQuantity(int quantity);
  		int getQuantity();
  		void record(string name,int points);
  		void seeRecords();

};
#endif
