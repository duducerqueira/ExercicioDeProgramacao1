#ifndef MENU_HPP
#define MENU_HPP

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "player.hpp"
#include "records.hpp"
using namespace std;
class Menu{
	bool open;
public:
	Menu();
  	~Menu();
  	void setOpen(bool open);
  	bool getOpen();
  	int gameMenu(Player * jog,Records * rec,int cont,int so );
};
#endif
