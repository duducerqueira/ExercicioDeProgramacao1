#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "gameObject.hpp"
#include <string>
#include <iostream>

using namespace std;

class Player:public GameObject{
	private:
		string name;
		bool alive;
		int points;
		bool won;
		int lifes;
	public:
		Player();
		~Player();

		string getName();
		void setName(string name);
		bool getAlive();
		void setAlive(bool alive);
		int getPoints();
		void setPoints(int points);
		bool getWon();
		void setWon(bool won);
		void choosePlayer();
		void setLifes(int lifes);
		int getLifes();
		void movement(char ** m,char movement);
		void positionsPlayer(char ** m);
		void result();
		void checkScore();
		void openPortal(char ** m);
};
#endif       
