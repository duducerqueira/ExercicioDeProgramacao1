#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "player.hpp"

using namespace std;
class Map{
	private:
  		int lines,columns;
	public:
  		Map();
  		~Map();
		int getLines();
		int getColumns();
		void readMap(char ** m);
		void positionsPlayer(Player * player,char ** m);
};
#endif
