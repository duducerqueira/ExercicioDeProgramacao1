#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "bonus.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "gameObject.hpp"
#include "map.hpp"
#include "menu.hpp"
#include "design.hpp"
#include "records.hpp"

using namespace std;

int positions(int mod){
  return rand()%mod;
}

int main(int argc, char ** argv){
  int cont=0,so=1;
  do{
      int movement,dif=0,control=0;
      char  ** m =(char**)malloc(sizeof(char*)*17);
      Player * jog = new Player();
      Trap * p[17];
      Bonus * bon[5];
      Menu * menu = new Menu();
      Map * map = new Map();
      Design * des = new Design();
      Records * rec = new Records();
      for(int i=0;i<5;i++){
        bon[i] = new Bonus();
      }
      menu->setOpen(true);
      dif = menu->gameMenu(jog,rec,cont,so);
      system("clear");
      cout<<"[1]-Linux: ";
      cin>>so;
      system("clear");
      map->readMap(m);
      menu->setOpen(false);
      for(int i=0;i<dif;i++){
        p[i] = new Trap();
      }

      while (jog->getAlive() != false) {

        initscr();
        jog->positionsPlayer(m);

        for(int i=0;i<dif;i++){
        if(m[p[i]->getX()][p[i]->getY()] != '#'){
          if(m[p[i]->getX()][p[i]->getY()] != '|'){
            p[i]->choosePlayer();
            m[p[i]->getX()][p[i]->getY()]  = p[i]->getObject();
          }
        }
      }

      for(int i=0;i<5;i++){
        if(m[bon[i]->getX()][bon[i]->getY()] != '#' && m[bon[i]->getX()][bon[i]->getY()] != jog->getObject()){
          if(m[bon[i]->getX()][bon[i]->getY()] != '|' && m[bon[i]->getX()][bon[i]->getY()] != 'B'){
            if((bon[i]->getY()+1)%2 == 0){
              m[bon[i]->getX()][bon[i]->getY()]  = bon[i]->getObject();
            }
          }
        }
      }
      for(int i=0;i<5;i++){
        if(m[jog->getX()][jog->getY()-1] == '+'){
          jog->setPoints(jog->getPoints()+bon[i]->getBonus());
          m[jog->getX()][jog->getY()]=jog->getObject();
          m[jog->getX()][jog->getY()-1]='B';
          control=30;
        }
      }
      for(int i=0;i<dif;i++){
        if(p[i]->getX() == jog->getX() && p[i]->getY() == jog->getY()){
            jog->setLifes(jog->getLifes()-p[i]->getLoss());
            if(jog->getLifes() == 0){
              jog->setAlive(false);
            }
            break;
        }
      }

       refresh();
       clear();

      des->setScreen(true);
      des->draw(m,jog);

      for(int i=0;i<dif;i++){
        if(m[p[i]->getX()][p[i]->getY()] !='#'){
          if(m[p[i]->getX()][p[i]->getY()] !='|'){
            m[p[i]->getX()][p[i]->getY()] = ' ';
          }
        }
      }

      for(int i=0;i<5;i++){
        if(m[bon[i]->getX()][bon[i]->getY()] !='#'){
          if(m[bon[i]->getX()][bon[i]->getY()] !='|'){
            m[bon[i]->getX()][bon[i]->getY()] = ' ';
          }
        }
      }

      if(control == 30){
      for(int q=0;q<5;q++){
        for(int i=0;i<17;i++){
          for(int j=0;j<73;j++){
            bon[q]->setX(positions(17));
            bon[q]->setY(positions(72));
          }
        }
      }
      control = 0;
      }
      control++;

      for(int q=0;q<dif;q++){
        for(int i=0;i<17;i++){
          for(int j=0;j<73;j++){
            p[q]->setX(positions(17));
            p[q]->setY(positions(73));
          }
        }
      }

      movement = getch();
      noecho();
      jog->movement(m,movement);

      jog->openPortal(m);
      jog->checkScore();
      if(jog->getWon() == true)
        break;

      endwin();
   }
  endwin();
  if(jog->getWon() == true){
    rec->setQuantity(rec->getQuantity()+1);
    rec->record(jog->getName(),jog->getPoints());
  }
  jog->result();
  delete(m);
  delete(menu);

  delete(map);
  delete(des);
  delete(rec);
  cont++;
}while(1);

  return 0;
}
