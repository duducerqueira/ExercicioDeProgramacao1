#include "design.hpp"

Design::Design(){
  screen=false;
}
Design::~Design(){}
void Design::setScreen(bool screen){
  this->screen = screen;
}
bool Design::getScreen(){
  return screen;
}
void Design::draw(char ** m,Player * jog){
  for(int s=0;s<17;s++){
    for(int j=0;j<73;j++){
      if(m[s][j] != '-' && m[s][j]!='B'){
        printw("%c",m[s][j]);
      }else{
        printw(" ");
      }
    }
    printw("\n");
  }
  printw("\n________________________________________\nPoints:         %d\nLifes:          %d\nCharacter:     %c\n________________________________________\n",jog->getPoints(),jog->getLifes(),jog->getObject());
}
