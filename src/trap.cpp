#include "trap.hpp"
#include <iostream>

using namespace std;

Trap::Trap(){
	setX(3);
	setY(0);
	loss=1;
	setObject('X');
}

Trap::~Trap(){

}

void Trap::setLoss(int loss){
	this->loss = loss;
}

int Trap::getLoss(){
	return this->loss;
}

void Trap::choosePlayer(){
	setObject('X');
}
