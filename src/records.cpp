#include "records.hpp"

Records::Records(){
	setQuantity(0);
}
Records::~Records(){

}

void Records::setQuantity(int quantity){
	this->quantity = quantity;
}

int Records::getQuantity(){
	return this->quantity;
}
void Records::record(string name,int points){
	FILE * rec;
	rec = fopen("../obj/records.txt","a+");
	if(rec == NULL){
    		rec = fopen("./obj/records.txt","a+");
  	}else{
    		cout<<"Could not open file!"<< endl;
  	}
  	fprintf(rec, "%s\n%d\n",name.c_str(),points);
  	fclose(rec);
}
void Records::seeRecords(){

	char aux[100];
	int a=0;
    	FILE * rec ;
    	rec = fopen("../obj/records.txt","r");
    	if(rec == NULL){
        	rec = fopen("./obj/records.txt","r");
      		if(rec == NULL){
        		cout<<"Could not open file!\n";
      		}
    	}
	while(fgets(aux,sizeof(aux),rec) != NULL){
    		if(a == 1){
      			setQuantity(getQuantity()+1);
      			a=0;
    		}
    	cout<<aux;
    	a++;
  	}
	fclose(rec);
}
