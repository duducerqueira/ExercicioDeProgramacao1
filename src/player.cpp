#include "player.hpp"

Player::Player(){
	setX(1);
	setY(1);
	setAlive(true);
	setWon(false);
	setPoints(0);
	setLifes(3);
}

Player::~Player(){

}

int Player::getLifes(){
	return this->lifes;
}

void Player::setLifes(int lifes){
	this->lifes=lifes;
}

bool Player::getAlive(){
	return this->alive;
}

void Player::setAlive(bool alive){
	this->alive = alive;
}

string Player::getName(){
	return this->name;
}

void Player::setName(string name){
	this->name = name;
}

int Player::getPoints(){
	return this->points;
}

void Player::setPoints(int points){
	this->points = points;
}

bool Player::getWon(){
	return this->won;
}

void Player::setWon(bool won){
	this->won = won;
}

void Player::choosePlayer(){
	int option;
	cout<<"Choose a player\n"<<"[1]- @\n"<<"[2]- }\n"<<"[3]- >"<<endl;
	cout<<"Op:";
	cin>>option;
	switch (option) {
	   case 1:
	      setObject('@');
      	      break;
    	   case 2:
              setObject('}');
              break;
    	case 3:
              setObject('>');
              break;
  	}
}

void Player::movement(char ** m,char movement){
	if(m[getX()][getY()+1] != '#' && movement == 'd' && m[getX()][getY()+1] != '|'){
           setY(getY()+1);
    	   m[getX()][getY()-1] =' ';
  	}else if(m[getX()][getY()-1] != '#' && movement == 'a' && m[getX()][getY()-1] != '|'){
    	   setY(getY()-1);
    	   m[getX()][getY()+1] =' ';
  	}else if(m[getX()+1][getY()] !='#' && movement == 's' && m[getX()+1][getY()] != '|'){
    	   setX(getX()+1);
    	   m[getX()-1][getY()] =' ';
  	}else if(m[getX()-1][getY()] !='#' && movement == 'w' && m[getX()][getY()-1] != '|'){
    	   setX(getX()-1);
    	   m[getX()+1][getY()] =' ';
 	 }
}

void Player::result(){
	if(getWon() == false){
    	   system("clear");
    	   cout<<"###############\n      You lose!:( \n#########################";
  	}else{
    	   system("clear");
    	   cout<<"###############\n      Result   :) \n#########################";
  	}
 	cout<<"\n\nPoints obtained:   "<<getPoints()<<"\nLifes             "<<getLifes()<<"\n[Enter] back to menu!"<<endl;
 	setbuf(stdin,NULL);
  	getchar();
}

void Player::openPortal(char ** m){
	if(getPoints()>=10){
	   for(int i=16;i<=16;i++){
      	      for(int j=22;j<26;j++){
                 m[i][j] = ' ';
      	      }
     	   }
  	}
}

void Player::checkScore(){
	if(getPoints() >= 10 && getX() == 16 && (getY()==22 || getY()==23 || getY()==24 || getY()==25)){
    	   setWon(true);
  	}
}

void Player::positionsPlayer(char ** m){
	if(m[getX()][getY()] != '#'){
    	   m[getX()][getY()]  = getObject();
  	}
}
