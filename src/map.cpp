
#include "map.hpp"

Map::Map(){
	lines = 17;
	columns = 73;
}

Map::~Map(){

}

int Map::getLines(){
	return this->lines;
}

int Map::getColumns(){
	return this->columns;
}


void Map::readMap(char ** m){

	char aux[75];
	for(int i=0;i<17;i++){
    		m[i] = (char*)malloc(sizeof(char)*73);
	}

	FILE * map = fopen("./obj/map.txt","r");
	if(map == NULL){
  		map = fopen("../obj/map.txt","r");
  		if(map == NULL){
    			cout<<"Could not open file!"<<endl;
  	}
	}
int i=0;

while(fgets(aux,sizeof(aux),map) != NULL){
       for(int j=0;j<73;j++){
           m[i][j] = aux[j];
        }
    i++;
}
fclose(map);

}
