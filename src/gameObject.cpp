#include "gameObject.hpp"
#include <iostream>

GameObject::GameObject(){
	x=0;
	y=0;
}

GameObject::~GameObject(){

}

void GameObject::setX(int x){
	this->x=x;
}

int GameObject::getX(){
	return this->x;
}

void GameObject::setY(int y){
	this->y=y;
}

int GameObject::getY(){
	return this->y;
}

void GameObject::setObject(char o){
	this->object=o;
}

char GameObject::getObject(){
	return this->object;
}

void GameObject::choosePlayer(){

}

